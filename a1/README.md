> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Application Development 

## Alexis Wood 

### Assignment #1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Developmental Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of running http://localhost9999 (#2 above, Step #4(b) in tutorial;
* git commands w/short descriptions;
* Bitbucket repo links: a) this assignment and b) the completed tutorial above (bitbucketstationlocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - initializes a git repository 
2. git status - shows status of files in the index vs working directory 
3. git add - adds files changes in your working directory to your index 
4. git commit - takes all commands written in the index and creates a new commit object pointing to it and sets new branch pointing to it 
5. git push - pushes all modified local objects to the remote repository and merges with local 
6. git pull - fetches fies from remote repository and merges with local 
7. git clone - makes a git repository copy from a remote source 

#### Assignment Screenshots:

*Screenshot of Tomcat running http://localhost*:

![AMPPS Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/helloworld.png)




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
