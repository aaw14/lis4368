
>

# LIS43868

## Alexis Wood

### A5# Requirements:

*Four Requirements:*

1. Create Basic Server Side Validation
2. Prepared statements to help prevent SQL Injection
3. JSTL to prevent XSS 
4. Add insert functionality to A4

#### README.md file should include the following items:


* [Validation](http://localhost:9999/lis4368/customerform.jsp?assign_num=a5)



> 

>


#### Assignment Screenshots:

*Screenshot of Basic Server Side Validation*
*Screenshot of Passed Validation http://localhost:9999/lis4368/index.jsp*:


![Validation Screenshot](img/a5a.png)
![Screenshot](img/a5b.png)
![Data Screenshot](img/a5c.png)





