
>

# LIS43868

## Alexis Wood

### P2# Requirements:

*Four Requirements:*

1. Create Basic Server Side, Client Side Validation
2. Prepared statements to help prevent SQL Injection
3. JSTL to prevent XSS 
4. Perform CRUD Functionality 

#### README.md file should include the following items:


* [Validation](http://localhost:9999/lis4368/customerAdmin)



> 

>


#### Assignment Screenshots:

*Screenshot of Pre Valid user form entry*
*Screenshot of Post Valid user form entry*
*Screenshots of MySQL customer table entry*


![Validation](img/p2a.png)
![Validation Screenshot](img/p2b.png)
![Passed Validation Screenshot](img/p2c.png)
![Modify Screenshot](img/p2d.png)
![Delete Screenshot](img/p2e.png)
![Data Screenshot](img/p2f.png)
![Data Screenshot](img/p2g.png)







