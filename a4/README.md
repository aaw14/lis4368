
>

# LIS43868

## Alexis Wood

### A4# Requirements:

*Four Requirements:*

1. Create Basic Server Side Validation
2. Tested jQuery Validation and used HTML5 to properly limit the number of characters per control 
3. Modify My Online Portfolio 
4. Chapter Questions (11-12)

#### README.md file should include the following items:


* [Validation](http://localhost:9999/lis4368/customerform.jsp?assign_num=a4)



> 

>


#### Assignment Screenshots:

*Screenshot of Basic Server Side Validation*
*Screenshot of Passed Validation http://localhost:9999/lis4368/index.jsp*:


![Validation Screenshot](img/a4a.png)
![Online Portfolio Screenshot](img/a4b.png)





