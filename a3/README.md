
>

# LIS43868

## Alexis Wood

### Assignment 3# Requirements:

*Three Requirements:*

1. Distributed Version of Git and Bitbucket
2. Screenshot of ERD 
3. Links to a3.mwb and a3.sql

#### README.md file should include the following items:


* [a3.sql](docs/a3.sql)
* [a3.mwb](docs/a3.mwb)



> 

>


#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/a3.png)





