> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Application Development 

## Alexis Wood 

### Course Work links 



*Screenshot of AMPPS running http://localhost*:

[A1 README.md](a1/README.md "My A1 README.md file")

* Install .NET Core 
* Create hwapp application 
* Create aspnetcoreapp application 
* Provide screenshots of installations 
* Create Bitbucket repo
* Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
* Provide git command descriptions 

 [A2 README.md](a2/README.md "My A2 README.md file")
 
* Distributed Version of Git and Bitbucket
* MySQL Development 
* Execute SQL Statements 

 [A3 README.md](a3/README.md "My A3 README.md file")

* Create ERD based upon business rules
* Provide screenshot of completed ERD
* Provide DB resource links 

 [P1 README.md](p1/README.md "My P1 README.md file")

* Create My Online Portfolio 
* Tested jQuery Validation and used HTML5 to properly limit the number of characters per control 
* Create Basic Client Side Validation 

 [A4 README.md](a4/README.md "My A4 README.md file")

* Create Basic Server Side Validation
* Tested jQuery Validation and use HTML5 properly 
* Keep My Online Portfolio up to date 

 [A5 README.md](a5/README.md "My A5 README.md file")

* Create Basic Server Side Validation
* Prepared statements to help prevent SQL Injection
* Add insert functionality to A4

 [P2 README.md](p2/README.md "My P2 README.md file")

* MVC Framework using basic client server side validation
* Prepared statements to help prevent SQL Injection
* Complete Crud Functionality
